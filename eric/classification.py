"""Classifier demonstration.

Adapted from sklearn example "Classifier Comparison"
http://scikit-learn.org/stable/auto_examples/plot_classifier_comparison.html

"""
import time

from sklearn.preprocessing import StandardScaler
from sklearn.tree import DecisionTreeClassifier
from sklearn.lda import LDA
from sklearn.svm import SVC

from data import load_data

# You should change this to wherever you download the data, which lives at
# https://files.nyu.edu/ejh333/public/uiowa_instrument_dataset.tgz
instrument_directory = "/Users/ejhumphrey/Downloads/instrument_dataset/"
train, test = load_data(instrument_directory)

# Initialize the classifiers.
names = [
    "Decision Trees",
    "LDA",
    "SVM"]
classifiers = [
    DecisionTreeClassifier(max_depth=5),
    LDA(),
    SVC(kernel="linear")]

# Preprocess the data.
X_train, y_train = train
X_test, y_test = test
scaler = StandardScaler()
X_train = scaler.fit_transform(X_train)
X_test = scaler.transform(X_test)

# Iterate over classifiers and print the results.
for name, clf in zip(names, classifiers):
    start_time = time.time()
    clf.fit(X_train, y_train)
    train_acc = clf.score(X_train, y_train)*100
    test_acc = clf.score(X_test, y_test)*100
    run_time = time.time() - start_time
    print "%s: Train Acc: %0.3f\t Test Acc: %0.3f in %0.2f seconds" \
        % (name, train_acc, test_acc, run_time)
