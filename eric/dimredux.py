"""Dimensionality reduction demonstration.

Adapted from sklearn example "Comparison of Manifold Learning methods"
http://scikit-learn.org/stable/auto_examples/manifold/plot_compare_methods.html


Note that the instrument map is:

  Flute: 0
  Oboe: 1
  Bassoon: 2
  Alto Saxophone: 3
  Bb Clarinet: 4
  Cello: 5
  Violin: 6
  Piano: 7
  Tenor Trombone: 8
  Trumpet: 9
"""

import numpy as np
import time
from matplotlib.pyplot import figure, show
from matplotlib.ticker import NullFormatter

from sklearn.preprocessing import StandardScaler
from sklearn.decomposition import PCA
from sklearn.manifold import Isomap, LocallyLinearEmbedding
from sklearn.lda import LDA

from data import load_data

# You should change this to wherever you download the data, which lives at
# https://files.nyu.edu/ejh333/public/uiowa_instrument_dataset.tgz
instrument_directory = "/Users/ejhumphrey/Downloads/instrument_dataset/"

# Constants for the file
train_points = 4000
test_points = 500
n_neighbors = 15
n_components = 2

# Load the data
train, test = load_data(instrument_directory)

# Randomly subsample the training set.
subidx = np.random.permutation(len(train[0]))[:train_points]
X_train, y_train = train[0][subidx], train[1][subidx]

# Randomly subsample the holdout set.
subidx = np.random.permutation(len(test[0]))[:test_points]
X_test, y_test = test[0][subidx], test[1][subidx]

# Preprocess the data.
scaler = StandardScaler()
X_train = scaler.fit_transform(X_train)
X_test = scaler.transform(X_test)

# Instantiate the reduction methods
labels = ['PCA', 'IsoMap', 'LLE', 'LDA']
methods = [
    PCA(n_components=n_components),
    Isomap(n_neighbors, n_components),
    LocallyLinearEmbedding(n_neighbors, n_components, eigen_solver='auto'),
    LDA(n_components=n_components)
]

# Create the figure object
fig_idx = 0
fig = figure(figsize=(8, 8))

# Iterate over the methods, plotting along the way
for name, redux in zip(labels, methods):
    # Learn and project into the lower dimensionality
    start_time = time.time()
    if name == 'LDA':
        redux.fit(X_train, y_train)
    else:
        redux.fit(X_train)
    Z_test = redux.transform(X_test)
    run_time = time.time() - start_time
    print "%s: Finished in %0.2f seconds" % (name, run_time)

    # Plot it!
    fig_idx += 1
    ax = fig.add_subplot(220 + fig_idx)
    ax.scatter(Z_test[:, 0], Z_test[:, 1], c=(y_test/10.0))
    ax.set_title("%s" % name)
    ax.xaxis.set_major_formatter(NullFormatter())
    ax.yaxis.set_major_formatter(NullFormatter())
    ax.axis('tight')

show()
