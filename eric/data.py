import numpy as np
import sklearn as sk
import os

FILE_FMT = "uiowa_%s_%s.npy"


def load_data(instrument_directory):
    """Given the base instrument directory, load the data into memory:

    Returns:
    train_data : tuple
        Train data/labels.
    test_data : tuple
        Test data/labels
    """
    data = dict()
    for split in ['train', 'test']:
        data[split] = []
        for key in ['data', 'labels']:
            np_file = os.path.join(instrument_directory,
                                   FILE_FMT % (split, key))
            data[split] += [np.load(np_file)]

    return data['train'], data['test']
