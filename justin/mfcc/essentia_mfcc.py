#!/usr/bin/env python
# encoding: utf=8

"""
essentia_mfcc.py

Example of using essentia in standard mode to compute MFCCs

Created by Gerard Roma
http://essentia.upf.edu/documentation/python_tutorial.html
Modified by Justin Salamon
"""

# first, we need to import our essentia module. It is aptly named 'essentia'!

import essentia
# as there are 2 operating modes in essentia which have the same algorithms,
# these latter are dispatched into 2 submodules:
import essentia.standard
import essentia.streaming

# pylab contains the plot() function, as well as figure, etc... (same names as Matlab)
from pylab import plot, show, figure, imshow

# let's have a look at what is in there
# print dir(essentia.standard)
# you can also do it by using autocompletion in IPython, typing "essentia.standard." and pressing Tab

# Essentia has a selection of audio loaders:
#
#  - AudioLoader: the basic one, returns the audio samples, sampling rate and number of channels
#  - MonoLoader: which returns audio, down-mixed and resampled to a given sampling rate
#  - EasyLoader: a MonoLoader which can optionally trim start/end slices and rescale according
#                to a ReplayGain value
#  - EqloudLoader: an EasyLoader that applies an equal-loudness filtering on the audio

# we start by instantiating the audio loader
# by default, the MonoLoader will output audio with 44100Hz samplerate
loader = essentia.standard.MonoLoader(filename = '../song.mp3')

# and then we actually perform the loading:
audio = loader()

plot(audio[1*44100:2*44100])
show() # unnecessary if you started "ipython --pylab"

#--------------------------------------------------------------------------------------------------------------

from essentia.standard import *
w = Windowing(type = 'hann')
spectrum = Spectrum()  # FFT() would return the complex FFT, here we just want the magnitude spectrum
mfcc = MFCC()

# let's have a look at the inline help:
# help(MFCC)
# you can also see it by typing "MFCC?" in IPython

# We can use essentia algorithms like normal functions:
# frame = audio[5*44100 : 5*44100 + 1024]
# spec = spectrum(w(frame))
# plot(spec)
# show() # unnecessary if you started "ipython --pylab"

# Compute MFCCs as we would using matlab, slicing the frames manually
# mfccs = []
# frameSize = 1024
# hopSize = 512
#
# for fstart in range(0, len(audio)-frameSize, hopSize):
#     frame = audio[fstart:fstart+frameSize]
#     mfcc_bands, mfcc_coeffs = mfcc(spectrum(w(frame)))
#     mfccs.append(mfcc_coeffs)
#
# # and plot them...
# # as this is a 2D array, we need to use imshow() instead of plot()
# imshow(mfccs, aspect = 'auto')
# show() # unnecessary if you started "ipython --pylab"

# Compute MFCCs exploiting the essentia FrameGenerator

mfccs = []

for frame in FrameGenerator(audio, frameSize = 1024, hopSize = 512):
    mfcc_bands, mfcc_coeffs = mfcc(spectrum(w(frame)))
    mfccs.append(mfcc_coeffs)

# transpose so that time is on the x axis in the plot
# we need to convert the list to an essentia.array first (== numpy.array of floats)
mfccs = essentia.array(mfccs).T

# and plot
imshow(mfccs[1:,:], aspect = 'auto', interpolation='nearest')
show() # unnecessary if you started "ipython --pylab"
# We ignored the first MFCC coefficient to disregard the power of the signal and only plot its spectral shape

#--------------------------------------------------------------------------------------------------------------

pool = essentia.Pool()

for frame in FrameGenerator(audio, frameSize = 1024, hopSize = 512):
    mfcc_bands, mfcc_coeffs = mfcc(spectrum(w(frame)))
    pool.add('lowlevel.mfcc', mfcc_coeffs)
    pool.add('lowlevel.mfcc_bands', mfcc_bands)

imshow(pool['lowlevel.mfcc'].T[1:,:], aspect = 'auto', interpolation= 'nearest')
show() # unnecessary if you started "ipython --pylab"
figure()
imshow(pool['lowlevel.mfcc_bands'].T, aspect = 'auto', interpolation = 'nearest')

output = YamlOutput(filename = 'mfcc.yaml') # use "format = 'json'" for JSON output
output(pool)

# or as a one-liner:
# YamlOutput(filename = 'mfcc.sig')(pool)

#--------------------------------------------------------------------------------------------------------------

# compute mean and variance of the frames
aggrPool = PoolAggregator(defaultStats = [ 'mean', 'var' ])(pool)

print 'Original pool descriptor names:'
print pool.descriptorNames()
print
print 'Aggregated pool descriptor names:'
print aggrPool.descriptorNames()

# and ouput those results in a file
YamlOutput(filename = 'mfccaggr.yaml')(aggrPool)