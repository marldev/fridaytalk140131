#!/usr/bin/env python
# encoding: utf=8

"""
essentia_beat_tracking.py

Example of using essentia in streaming mode to detect the beats of a piece of music
Created by Justin Salamon
"""

import sys
import essentia
from essentia.streaming import *
from essentia.standard import YamlOutput

try:
    infile = sys.argv[1]
    outfile = sys.argv[2]
except:
    print "usage:", sys.argv[0], "<input audio file> <output json file>"
    sys.exit()

# initialize algorithms we will use
loader = MonoLoader(filename=infile)
beattracker = BeatTrackerMultiFeature()

pool = essentia.Pool()

loader.audio >> beattracker.signal
beattracker.ticks >> (pool,'beats.ticks')
beattracker.confidence >> (pool,'beats.confidence')

print 'Running network...'
essentia.run(loader)

print 'Saving beats to json file...'
YamlOutput(filename=outfile,format='json')(pool)

print 'Writing audio file to disk with beats...'
marker = essentia.standard.AudioOnsetsMarker(onsets = pool['beats.ticks'], type = 'beep')
loader2 = essentia.standard.MonoLoader(filename=infile)
audio = loader2.compute()
marked_audio = marker(audio)
essentia.standard.MonoWriter(filename = 'beats.wav')(marked_audio)