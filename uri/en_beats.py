#!/usr/bin/env python
"""
Uses The Echo Nest Remix API in order to obtain the beats of the
wav files contained in a given folder, and stores them into
JSON files.
"""

import argparse
import glob
import json
import numpy as np
import os
import time

# These lines import specific things from Remix! 
from echonest.remix.audio import LocalAudioFile

# Set Uri's Echo Nest API Key
from pyechonest import config
config.ECHO_NEST_API_KEY="RRIGOW0BG5NJTM5M4"

def save_beats(beats, outfile):
    """Save the beats into a json formatted file."""
    fp = open(outfile, "w")
    out_beats = {}
    out_beats["beats"] = []
    for beat in beats:
        out_start = {}
        out_start["start"] = beat.start
        out_beats["beats"].append(out_start)
    json.dump(out_beats, fp)
    fp.close()

def main(args):
    """For each file in the folder, process the beats."""
    start_time = time.time()
    wavfiles = glob.glob(os.path.join(args.wavfolder, "*.wav"))
    for wavfile in wavfiles:
        track = LocalAudioFile(wavfile, verbose=True)
        beats = track.analysis.beats
        save_beats(beats, wavfile.replace(".wav", ".json"))

    print "Done! Took %.1f seconds." % (time.time() - start_time)

if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        description="Estimates the beats of the tracks of a specific folder " \
                    "by using the Echo Nest Analyzer and saves them in a " \
                    "json format")
    parser.add_argument("wavfolder",
                        metavar="wavfolder", type=str,
                        help="The path to the folder with wav files to " \
                             "analyze.")
    main(parser.parse_args())